import { writable } from "svelte/store";
import { dev as devMode } from "$app/environment";

function localStore(key, initialValue) {
  const initial =
    typeof window !== "undefined" && localStorage.getItem(key)
      ? JSON.parse(localStorage.getItem(key))
      : initialValue;
  const store = writable(initial);

  store.subscribe((value) => {
    if (typeof window !== "undefined") {
      localStorage.setItem(key, JSON.stringify(value));
    }
  });

  return store;
}

//  LocalStore stores
export const pipedInstancePingResults = localStore(
  "pipedInstancePingResults",
  []
);
export const tokenType = localStore("tokenType", "");
export const accessToken = localStore("accessToken", "");
export const expiresIn = localStore("experationTime", "");

export const playlists = localStore("playlists", {});
export const tracks = localStore("tracks", {});

//  Not localStore stores
export const preloaded = writable(false);
export let loadingStatus = writable("Loading...");

// Not Stores, just important variables
const apiURL = "https://accounts.spotify.com/authorize";
const clientID = "cfbe41dd7acd4d98801add2537365514";
const redirectURI = devMode
  ? "http://localhost:5173/callback"
  : "https://cprhythm.davidstephenson.net/callback";
const scope =
  "playlist-read-private playlist-read-collaborative user-library-read";

export const loginURL = `${apiURL}?response_type=token&client_id=${clientID}&scope=${encodeURIComponent(
  scope
)}&redirect_uri=${encodeURIComponent(redirectURI)}`;
