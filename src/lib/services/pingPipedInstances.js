const instances = [
  "https://piped-api.garudalinux.org",
  "https://piapi.ggtyler.dev",
  "https://pipedapi.drgns.space",
  "https://pipedapi.simpleprivacy.fr",
  "https://pipedapi.osphost.fi",
  "https://api.piped.yt",
  "https://pipedapi.syncpundit.io",
  "https://api-piped.mha.fi",
  "https://pipedapi.leptons.xyz",
  "https://ytapi.dc09.ru",
  "https://api.looleh.xyz",
  "https://watchapi.whatever.social",
];

const testVideoID = "dQw4w9WgXcQ";

export async function pingPipedInstances() {
  const results = [];
  const maxConcurrency = 6;

  const sendRequest = async (instance) => {
    const start = performance.now();
    const url = `${instance}/streams/${testVideoID}`;

    try {
      const responsePromise = fetch(url, { mode: "no-cors" });

      // Create a timeout promise for this request
      const timeoutPromise = new Promise((resolve, reject) => {
        setTimeout(() => {
          reject(new Error("Request timeout (3 seconds exceeded)"));
        }, 3000);
      });

      // Race the response and timeout promises
      const response = await Promise.race([responsePromise, timeoutPromise]);

      const end = performance.now();
      const responseTime = end - start;

      console.log(`${instance} responded in ${responseTime}ms`);
      results.push({
        instance,
        responseTime,
      });
    } catch (error) {
      // Handle fetch error or timeout error here
      console.error(`Request to ${instance} failed: ${error.message}`);
    }
  };

  // Use a loop to control concurrency
  for (let i = 0; i < instances.length; i += maxConcurrency) {
    const currentBatch = instances.slice(i, i + maxConcurrency);

    // Use Promise.all to send requests in parallel within the current batch
    await Promise.all(currentBatch.map(sendRequest));
  }

  results.sort((a, b) => a.responseTime - b.responseTime);

  return results;
}
