import { pingPipedInstances } from "$lib/services/pingPipedInstances";
import { pipedInstancePingResults, preloaded, loadingStatus } from "$lib/stores";

export async function preload() {
  loadingStatus.set("Pinging Piped Instances...");
  await Promise.all([
    new Promise((resolve) => {
      pipedInstancePingResults.subscribe((value) => {
        if (value.length === 0) {
          pingPipedInstances().then((results) => {
            pipedInstancePingResults.set(results);
            resolve();
          });
        } else {
          resolve();
        }
      });
    }),
  ]);

  preloaded.set(true);
}
