import { tokenType, accessToken, loadingStatus } from "$lib/stores";
import { getTracks } from "./getTracks";

let type, token;

tokenType.subscribe((value) => {
  type = value;
});

accessToken.subscribe((value) => {
  token = value;
});

export async function getPlaylists() {
  const playlists = {};
  let offset = 0;
  let total;

  async function fetchPlaylists() {
    const response = await fetch(
      `https://api.spotify.com/v1/me/playlists?limit=20&offset=${offset}`,
      {
        headers: {
          Authorization: `${type} ${token}`,
        },
      }
    );

    if (!response.ok) {
      console.error(`Error fetching playlists: ${response.statusText}`);
      return;
    }

    const data = await response.json();

    loadingStatus.set(`Scanning playlists`);
    for (const playlist of data.items) {
      let playlistData = {
        name: playlist.name,
        id: playlist.id,
        image: playlist.images[0].url,
        tracks: await getTracks(playlist.id),
      };

      playlists[playlist.id] = playlistData;
    }

    offset += 20;
    total = data.total;
  }

  while (total === undefined || total > offset) {
    await fetchPlaylists();
  }

  return playlists;
}
