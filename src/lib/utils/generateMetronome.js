export async function generateMetronome(audioContext, BPM, durationMS) {
  const duration = durationMS / 1000; // Convert duration in milliseconds to seconds
  const frequency = 440; // Adjust the frequency of the beep as needed

  const audioBuffer = audioContext.createBuffer(1, audioContext.sampleRate * duration, audioContext.sampleRate);
  const channelData = audioBuffer.getChannelData(0);

  const beatsPerSecond = BPM / 60;
  const samplesPerBeat = audioContext.sampleRate / beatsPerSecond;

  for (let i = 0; i < audioBuffer.length; i++) {
    const t = i / audioContext.sampleRate;
    const beatIndex = Math.floor(t * beatsPerSecond);
    const isInBeat = t - beatIndex / beatsPerSecond < 0.1; // Adjust the duration of the beep

    channelData[i] = isInBeat ? Math.sin(2 * Math.PI * frequency * t) : 0;
  }

  // Create an AudioBufferSourceNode with the audio data
  const audioSource = audioContext.createBufferSource();
  audioSource.buffer = audioBuffer;

  return audioSource;
}
