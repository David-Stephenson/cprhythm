import { pipedInstancePingResults } from "$lib/stores.js";

let pipedInstances;

pipedInstancePingResults.subscribe((value) => {
  pipedInstances = value;
});

async function tryApiUrls(song, artist, urls) {
  for (const urlObj of urls) {
    const apiUrl = urlObj.instance;

    try {
      const query = `${song} - ${artist}`;
      const controller = new AbortController();
      const signal = controller.signal;

      const responsePromise = fetch(`${apiUrl}/search?q=${query}&filter=music_songs`, { signal });

      // Use Promise.race to handle the timeout for the search request
      const timeoutPromise = new Promise((_, reject) => {
        setTimeout(() => {
          controller.abort();
          reject(new Error(`API request to ${apiUrl} for search timed out`));
        }, 5000); // 5 seconds timeout for search
      });

      const response = await Promise.race([responsePromise, timeoutPromise]);

      if (response instanceof Response) {
        const data = await response.json();

        const audioUrl = data.items[0].url.replace("/watch?v=", "");
        const audioController = new AbortController();
        const audioSignal = audioController.signal;

        const audioResponsePromise = fetch(`${apiUrl}/streams/${audioUrl}`, { signal: audioSignal });

        const audioTimeoutPromise = new Promise((_, reject) => {
          setTimeout(() => {
            audioController.abort();
            reject(new Error(`API request to ${apiUrl} for /streams/${audioUrl} timed out`));
          }, 5000); // 5 seconds timeout for /streams request
        });

        const audioResponse = await Promise.race([audioResponsePromise, audioTimeoutPromise]);

        if (audioResponse instanceof Response) {
          const audioData = await audioResponse.json();
          return audioData["audioStreams"][0].url;
        } else {
          console.error(audioResponse.message);
        }
      } else {
        console.error(response.message);
      }
    } catch (error) {
      console.error(`API request to ${apiUrl} failed: ${error.message}`);
    }

    // Wait for a moment before trying the next API URL
    await new Promise((resolve) => setTimeout(resolve, 1000));
  }

  throw new Error("All API requests failed");
}

export async function searchYoutube(song, artist) {
  if (!pipedInstances || pipedInstances.length === 0) {
    throw new Error("No available API instances");
  }

  try {
    const audioUrl = await tryApiUrls(song, artist, pipedInstances);
    return audioUrl;
  } catch (error) {
    console.error(`All API requests failed: ${error.message}`);
    throw error;
  }
}
