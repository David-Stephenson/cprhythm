import { tokenType, accessToken, tracks } from "$lib/stores";

let type, token;

// Subscribe to stores
tokenType.subscribe((value) => {
  type = value;
});

accessToken.subscribe((value) => {
  token = value;
});

export async function getTracks(playlistID) {
  const newTracks = {};
  const response = await fetch(
    `https://api.spotify.com/v1/playlists/${playlistID}/tracks`,
    {
      headers: {
        Authorization: `${type} ${token}`,
      },
    }
  );

  if (!response.ok) {
    console.error(`Error fetching tracks: ${response.statusText}`);
    return null;
  }

  const data = await response.json();
  const trackIds = data.items.map((element) => element.track.id).join(",");

  // Fetch audio features
  const audioFeaturesResponse = await fetch(
    `https://api.spotify.com/v1/audio-features?ids=${trackIds}`,
    {
      headers: {
        Authorization: `${type} ${token}`,
      },
    }
  );

  if (!audioFeaturesResponse.ok) {
    console.error(
      `Error fetching audio features: ${audioFeaturesResponse.statusText}`
    );
    return null;
  }

  const audioFeaturesData = await audioFeaturesResponse.json();

  data.items.forEach((element) => {
    const trackId = element.track.id;
    const audioFeature = audioFeaturesData.audio_features.find(
      (feature) => feature.id === trackId
    );

    if (audioFeature) {
      newTracks[trackId] = {
        name: element.track.name,
        id: element.track.id,
        image: element.track.album.images[0].url,
        artist: element.track.artists[0].name,
        duration: element.track.duration_ms,
        bpm: audioFeature.tempo,
        cprSafe: audioFeature.tempo >= 100 && audioFeature.tempo <= 120,
      };
    }
  });

  // Update the tracks store with the new data
  tracks.update((currentTracks) => ({
    ...currentTracks,
    ...newTracks,
  }));

  return newTracks;
}
