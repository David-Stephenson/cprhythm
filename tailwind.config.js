export default {
  content: ["./src/**/*.{html,js,svelte,ts}"],
  theme: {
    extend: {},
  },
  daisyui: {
    themes: ["valentine"],
  },
  plugins: [require("daisyui")],
};
